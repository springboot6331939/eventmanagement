## Functional Requirements:
* Ability to add and remove a customer
* Ability to search for an event
* Ability to book an ticket for specific event.
* Ability to cancel a ticket.
* Ability to add an event. 
* Ability to cancel an event.
* Ability to track a event for the location and other kinds of information.
* Ability to generate reports on particular event(admin)
## Non Functional Requirements:
* User friendly UI.
* Security for the payment.
* Ability to handle multiple user.
* High performance and scalability.
## Entities
* User
* Ticket
* Admin
* Booking
* Event
* Return Booking
## Relationships
* A Management system can have multiple events.
* A event can have multiple Users
* A event can have multiple admins
* A Booking can have multiple users.
* A Return booking has multiple tickets
* A Management system can have multiple admins.
* A management system can have multiple users.