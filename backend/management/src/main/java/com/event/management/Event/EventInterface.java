package com.event.management.Event;

public interface EventInterface {
    Event delete(Event event);
    Event update(Event event);
    Event userAddition(Event event);
    Event ticketAddition(Event event);
}
