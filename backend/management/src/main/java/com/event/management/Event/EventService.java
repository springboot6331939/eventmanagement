package com.event.management.Event;

import java.util.NoSuchElementException;

public class EventService implements EventInterface{

    private EventRepository eventRepository;
    @Override
    public Event delete(Event event) {
        if(eventRepository.findById(event.getEvent_id())!=null){
            eventRepository.delete(event);
            return event;
        }
        return event;
    }
    @Override
    public Event update(Event event) throws NoSuchElementException {
        if(eventRepository.findById(event.getEvent_id()).isPresent())
        {
           
            eventRepository.save(event);
            return  eventRepository.findById(event.getEvent_id()).get();
        }
        return event;
    }

    @Override
    public Event userAddition(Event event) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'userAddition'");
    }

    @Override
    public Event ticketAddition(Event event) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'ticketAddition'");
    }
    

}
