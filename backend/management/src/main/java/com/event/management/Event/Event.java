package com.event.management.Event;

import java.util.List;


import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OrderColumn;
import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true)
    private long event_id;
    @Column(unique = true)
    private String event_name;
    private String event_organizer;
    private Long max_capacity;
    private Long number_of_users_registered;
    @ElementCollection
    @OrderColumn
    private List<Long> users_id;
    @ElementCollection
    @OrderColumn
    private List<String> types_of_tickets;
    public void addUsersToExisting(Long user_id){
        this.users_id.add(user_id);
    }
}
