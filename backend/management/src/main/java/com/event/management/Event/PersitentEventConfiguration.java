package com.event.management.Event;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    basePackages = "com.event.management.Event",
    entityManagerFactoryRef = "eventEntityManagerFactory",
    transactionManagerRef = "eventTransactionManager")
public class PersitentEventConfiguration {
    @Bean
    @ConfigurationProperties("spring.second-datasource")
    public DataSourceProperties serverDataSourceProperties(){
        return new DataSourceProperties();
    }
    @Bean
    @ConfigurationProperties("spring.second-datasource.configuration")
    public DataSource serverDataSource(){
        return serverDataSourceProperties().initializeDataSourceBuilder()
        .type(HikariDataSource.class).build();
    }
    @Bean(name = "eventEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean eventEntityManagerFactory(
        EntityManagerFactoryBuilder builder, @Qualifier("serverDataSource") DataSource dataSource){
            return builder
            .dataSource(dataSource)
            .packages(Event.class)
            .build();
    }
    @Bean(name = "eventTransactionManager")
    public PlatformTransactionManager eventTransactionManager(
        @Qualifier("eventEntityManagerFactory") LocalContainerEntityManagerFactoryBean eventEntityManagerFactory){
            return new JpaTransactionManager(eventEntityManagerFactory.getObject());
    }
    @Bean
public EntityManagerFactoryBuilder entityManagerFactoryBuilder() {
   return new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(), new HashMap<>(), null);
}
}
